#!/usr/bin/env python2.6
# Thesis Evaluation Form 
# Antal A. Buss <antal.buss@gmail.com>

from bottle import route, run, template, static_file, get, post, request, redirect, default_app 
import json
import os, sys
import subprocess

datapath='/var/www/tesisform/'
sys.path = [datapath] + sys.path
os.chdir(datapath)

@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='static/')

@route('/new')
def create_form():
    return template('new-form')


@post('/create')
def create():
    fdata = request.forms
#    data = {k:v for (k,v) in fdata.items()}
    data = {}
    for (k,v) in fdata.items():
        data[k]=v
    
    actn = data['actn']
    with open("actas/"+actn+".json","w") as f:
        json.dump(data,f)
    redirect('/tesisform/')


def calcgrade(data,item,per,pj1,pj2,pdir):
    tmpj1 = float(data['g'+item+"j1"])*pj1
    tmpj2 = float(data['g'+item+"j2"])*pj2
    tmpdir = float(data['g'+item+"dir"])*pdir
    return (tmpj1+tmpj2+tmpdir)*per


@post('/save')
def save_data():
    fdata = request.forms
#    data = {k:v for (k,v) in fdata.items()}
    data = {}
    for (k,v) in fdata.items():
        data[k]=v

    actn = data['actn']
    data['gA']  = calcgrade(data,'A', 0.05,0.35,0.35,0.30)
    data['gB1'] = calcgrade(data,'B1',0.05,0.35,0.35,0.30)
    data['gB2'] = calcgrade(data,'B2',0.05,0.35,0.35,0.30)
    data['gC']  = calcgrade(data,'C', 0.12,0.35,0.35,0.30)
    data['gD']  = float(data['gDdir'])*0.08
    data['gE']  = calcgrade(data,'E', 0.10,0.35,0.35,0.30)
    data['gF1'] = calcgrade(data,'F1',0.06,0.35,0.35,0.30)
    data['gF2'] = calcgrade(data,'F2',0.06,0.35,0.35,0.30)
    data['gG1'] = calcgrade(data,'G1',0.05,0.35,0.35,0.30)
    data['gG2'] = calcgrade(data,'G2',0.05,0.35,0.35,0.30)
    data['gH']  = float(data['gHdir'])*0.11
    data['gI']  = calcgrade(data,'I', 0.05,0.35,0.35,0.30)
    data['gJ1'] = calcgrade(data,'J1',0.06,0.35,0.35,0.30)
    data['gJ2'] = calcgrade(data,'J2',0.06,0.35,0.35,0.30)
    data['gK']  = calcgrade(data,'K', 0.05,0.35,0.35,0.30)
    gradedef = data['gA']+data['gB1']+data['gB2']+data['gC']+ \
        data['gD']+data['gE']+data['gF1']+data['gF2']+ \
        data['gG1']+data['gG2']+data['gH']+data['gI']+ \
        data['gJ1']+data['gJ2']+data['gK']
    data['gdef'] = "%.2f" % round(gradedef,2)

    with open("actas/"+actn+".json","w") as f:
        f.write(json.dumps(data,indent=2,sort_keys=True))
    redirect('/tesisform/pdf?actn=%s'%(actn))
    # return template('headtable', data)


@route('/')
def list_of_actas():
    files = os.listdir('actas')
#    print files
    reg = []
    for fname in files:
        with open("actas/"+fname,"r") as f:
            data = json.load(f)
            reg.append({'actn':data['actn'], 'titulo':data['titulo']})
    return template('list-trabajo-grado',listact=reg)


@route('/tesis')
def tesis_form():
    actn = request.query.actn
#    print actn
    fname = "actas/"+actn + ".json"
    with open(fname,"r") as f:
        data = json.load(f)
        return template('trabajo-grado-cs', data)


@route('/pdf')
def tesis_form():
    actn = request.query.actn
    fname = "actas/"+actn + ".json"
    with open(fname,"r") as f:
        data = json.load(f)
    tex = template('tesis-evaluation-tex', data)
    tex = tex.encode('utf-8')
    texfname = "%s" % (actn)
#    print texfname
    with open("tmp/"+actn+".tex","w") as f:
        f.write(tex)
    p = subprocess.check_call(["./runtex", texfname])
    # return static_file(actn+".pdf", root='tmp', download=actn+".pdf")
    return static_file(actn+".pdf", root='tmp')

#bottle.
application = default_app()

#run(host="0.0.0.0", port=8080, debug=True)

