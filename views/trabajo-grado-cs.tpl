<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Evaluación the tesis de grado</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

    <style>
    #notadef {
        color: blue;
        display: inline;
        font-weight: bolder;
        font-size: 1.5em;
    }
    .ui-widget {
        font-family: Georgia, "Times New Roman", Times, serif;
        font-size: 1em
    }

    .item {
       color: blue;
    }
    </style>

    <script>
    $(function() {
        // run the currently selected effect
        function runEffect() {
            // get effect type from
            var selectedEffect = "blind";
 
            // most effect types need no options passed by default
            var options = {};
 
            // run the effect
            $( "#tablanotas" ).toggle( selectedEffect, options, 500 );
        };
 
        // set effect from select menu value
        $( "#vertabla" ).click(function() {
            runEffect();
            return false;
        });
    });
    </script>
    <script>
      $(function() {
        $( "#calificaciones" ).accordion();
      });
    </script>

    <script>
    var calcgrade = function(item,per,pj1,pj2,pdir) {
        var tmpj1 = (document.res[item+"j1"].value)*pj1;
        var tmpj2 = (document.res[item+"j2"].value)*pj2;
        var tmpdir = (document.res[item+"dir"].value)*pdir;
        return (tmpj1+tmpj2+tmpdir)*per;
    };

    var updateTbl = function(event){
        document.res.Aj1.value=document.data.gAj1.value;
        document.res.Aj2.value=document.data.gAj2.value;
        document.res.Adir.value=document.data.gAdir.value;
        document.res.notaA.value = calcgrade('A', 0.05,0.35,0.35,0.30);
        
        document.res.B1j1.value=document.data.gB1j1.value;
        document.res.B1j2.value=document.data.gB1j2.value;
        document.res.B1dir.value=document.data.gB1dir.value;
        document.res.notaB1.value = calcgrade('B1',0.05,0.35,0.35,0.30);

        document.res.B2j1.value=document.data.gB2j1.value;
        document.res.B2j2.value=document.data.gB2j2.value;
        document.res.B2dir.value=document.data.gB2dir.value;
        document.res.notaB2.value = calcgrade('B2',0.05,0.35,0.35,0.30);

        document.res.Cj1.value=document.data.gCj1.value;
        document.res.Cj2.value=document.data.gCj2.value;
        document.res.Cdir.value=document.data.gCdir.value;
        document.res.notaC.value = calcgrade('C',0.12,0.35,0.35,0.30);

        document.res.Ddir.value = document.data.gDdir.value;
        document.res.notaD.value = document.data.gDdir.value*0.08;

        document.res.Ej1.value=document.data.gEj1.value;
        document.res.Ej2.value=document.data.gEj2.value;
        document.res.Edir.value=document.data.gEdir.value;
        document.res.notaE.value = calcgrade('E',0.10,0.35,0.35,0.30);

        document.res.F1j1.value=document.data.gF1j1.value;
        document.res.F1j2.value=document.data.gF1j2.value;
        document.res.F1dir.value=document.data.gF1dir.value;
        document.res.notaF1.value = calcgrade('F1',0.06,0.35,0.35,0.30);

        document.res.F2j1.value=document.data.gF2j1.value;
        document.res.F2j2.value=document.data.gF2j2.value;
        document.res.F2dir.value=document.data.gF2dir.value;
        document.res.notaF2.value = calcgrade('F2',0.06,0.35,0.35,0.30);

        document.res.G1j1.value=document.data.gG1j1.value;
        document.res.G1j2.value=document.data.gG1j2.value;
        document.res.G1dir.value=document.data.gG1dir.value;
        document.res.notaG1.value = calcgrade('G1',0.05,0.35,0.35,0.30);

        document.res.G2j1.value=document.data.gG2j1.value;
        document.res.G2j2.value=document.data.gG2j2.value;
        document.res.G2dir.value=document.data.gG2dir.value;
        document.res.notaG2.value = calcgrade('G2',0.05,0.35,0.35,0.30);

        document.res.Hdir.value = document.data.gHdir.value;
        document.res.notaH.value = document.data.gHdir.value*0.11;

        document.res.Ij1.value=document.data.gIj1.value;
        document.res.Ij2.value=document.data.gIj2.value;
        document.res.Idir.value=document.data.gIdir.value;
        document.res.notaI.value = calcgrade('I',0.05,0.35,0.35,0.30);

        document.res.J1j1.value=document.data.gJ1j1.value;
        document.res.J1j2.value=document.data.gJ1j2.value;
        document.res.J1dir.value=document.data.gJ1dir.value;
        document.res.notaJ1.value = calcgrade('J1',0.06,0.35,0.35,0.30);

        document.res.J2j1.value=document.data.gJ2j1.value;
        document.res.J2j2.value=document.data.gJ2j2.value;
        document.res.J2dir.value=document.data.gJ2dir.value;
        document.res.notaJ2.value = calcgrade('J2',0.06,0.35,0.35,0.30);

        document.res.Kj1.value=document.data.gKj1.value;
        document.res.Kj2.value=document.data.gKj2.value;
        document.res.Kdir.value=document.data.gKdir.value;
        document.res.notaK.value = calcgrade('K',0.05,0.35,0.35,0.30);
        
        var tmpdef = 
            parseFloat(document.res.notaA.value) +
            parseFloat(document.res.notaB1.value) +
            parseFloat(document.res.notaB2.value) + 
            parseFloat(document.res.notaC.value) +
            parseFloat(document.res.notaD.value) + 
            parseFloat(document.res.notaE.value) +
            parseFloat(document.res.notaF1.value) + 
            parseFloat(document.res.notaF2.value) +
            parseFloat(document.res.notaG1.value) + 
            parseFloat(document.res.notaG2.value) +
            parseFloat(document.res.notaH.value) + 
            parseFloat(document.res.notaI.value) + 
            parseFloat(document.res.notaJ1.value) +
            parseFloat(document.res.notaJ2.value) + 
            parseFloat(document.res.notaK.value);

        // document.getElementById('notadef').innerText = Math.round(tmpdef*100)/100;
        $('#notadef')[0].innerText = Math.round(tmpdef*100)/100;
    };
    </script>
</head>
<body>

%include header
<hr>
<h2>ACTA DE CALIFICACIÓN DE TRABAJO DE GRADO</h2>

%include headtable actn=actn, fecha=fecha, titulo=titulo, aut1=aut1, aut2=aut2, dir=dir, jur1=jur1, jur2=jur2


<form name="data" action="save" id="grades" method="POST">

%include headinfo actn=actn, fecha=fecha, titulo=titulo, aut1=aut1, aut2=aut2, dir=dir, jur1=jur1, jur2=jur2

<p>En atención al desarrollo de este Trabajo de Grado y al documento y
sustentación que presentaron los autores, los jurados damos las
siguientes calificaciones parciales y observaciones:</p>
 
<div id="calificaciones">
<h3>(A) Habilidad para aplicar conocimientos de matemáticas, ciencias
e ingeniería</h3>
<div id="item">
%include inputGrades pid='A', vj1=get('gAj1',"0"), vj2=get('gAj2',"0"), vdir=get('gAdir',"0")
Desarrollo, profundidad y rigor científico en el tratamiento del
tema. Interpretación de los resultadosobtenidos.<br/>
<i>Observaciones:</i><br/>
<textarea name="obsA" rows="3" cols="80" >{{get('obsA','')}}</textarea>
</div>


<h3>(B) Habilidad para analizar un problema e identificar los
requerimientos necesarios para su definición y solución</h3>
<div id="item">
%include inputGrades pid='B1', vj1=get('gB1j1',"0"), vj2=get('gB1j2',"0"), vdir=get('gB1dir',"0") 
(B1) Análisis del problema a solucionar y formulación de hipótesis.<br/> 

%include inputGrades pid='B2', vj1=get('gB2j1',"0"), vj2=get('gB2j2',"0"), vdir=get('gB2dir',"0") 
(B2) Recopilación  bibliográfica del estado del arte del problema<br/>
<i>Observaciones:</i><br>
<textarea name="obsB" rows="3" cols="80" >{{get('obsB','')}}</textarea>
</div>


<h3>(C) Habilidad para diseñar, implementar y evaluar procesos y 
sistemas computacionales</h3>
<div>
%include inputGrades pid='C', vj1=get('gCj1',"0"), vj2=get('gCj2',"0"), vdir=get('gCdir',"0") 
Diseño y creatividad de la solución propuesta<br/>
<i>Observaciones:</i><br>
<textarea name="obsC" rows="3" cols="80" >{{get('obsC','')}}</textarea>
</div>


<h3>(D) Habilidad para funcionar en equipos de trabajo</h3>
<div>
<input name="gDj1" size="5" readonly title="Jurado 1" value="N/A">
<input name="gDj2" size="5" readonly title="Jurado 2" value="N/A"> 
<input name="gDdir" size="5" type="number" step="0.1" min="0" max="5" title="Director" onBlur="document.res.ddir.value=document.data.gDdir.value" value="{{get('gDdir','0')}}"> 
Integración de diferentes puntos de vista para proponer soluciones<br/>
<i>Observaciones:</i><br>
<textarea name="obsD" rows="3" cols="80" >{{get('obsD','')}}</textarea>
</div>


<h3>(E) Entendimiento de la responsabilidad profesional y ética</h3>
<div>
%include inputGrades pid='E', vj1=get('gEj1',"0"), vj2=get('gEj2',"0"), vdir=get('gEdir',"0") 
Adecuado cumplimiento de los objetivos del proyecto y puntualidad en
las entregas<br/>
<i>Observaciones:</i><br>
<textarea name="obsE" rows="3" cols="80" >{{get('obsE','')}}</textarea>
</div>


<h3>(F) Habilidad para comunicarse efectivamente</h3>
<div>
%include inputGrades pid='F1', vj1=get('gF1j1',"0"), vj2=get('gF1j2',"0"), vdir=get('gF1dir',"0") 
(F1) Calidad y redacción del documento escrito.<br/> 

%include inputGrades pid='F2', vj1=get('gF2j1',"0"), vj2=get('gF2j2',"0"), vdir=get('gF2dir',"0") 
(F2) Solvencia de la presentación oral<br/>
<i>Observaciones:</i><br>
<textarea name="obsF" rows="3" cols="80" >{{get('obsF','')}}</textarea>
</div>


<h3>(G) Habilidad para analizar los impactos de la computación y la
ingeniería en las personas, organizaciones y la sociedad</h3>
<div>

%include inputGrades pid='G1', vj1=get('gG1j1',"0"), vj2=get('gG1j2',"0"), vdir=get('gG1dir',"0") 
(G1) Justificación del proyecto.<br/>

%include inputGrades pid='G2', vj1=get('gG2j1',"0"), vj2=get('gG2j2',"0"), vdir=get('gG2dir',"0") 
(G2) Identificación de los impactos de la solución en usuarios,
comunidad científica y/o aplicaciones derivadas<br/>

<i>Observaciones:</i><br>
<textarea name="obsG" rows="3" cols="80" >{{get('obsG','')}}</textarea>
</div>


<h3>(H) El reconocimiento de la necesidad de, y la habilidad para,
continuar con el desarrollo profesional</h3>
<div>
<input name="gHj1" size="5" readonly title="Jurado 1" value="N/A">
<input name="gHj2" size="5" readonly title="Jurado 2" value="N/A">
<input name="gHdir" size="5" type="number" step="0.1" min="0" max="5" title="Director" onBlur="document.res.hdir.value=document.data.gHdir.value" value="{{get('gHdir','0')}}">
Actitud para aprender nuevo conocimiento y desafío intelectual de la
solución propuesta<br/>
<i>Observaciones:</i><br>
<textarea name="obsH" rows="3" cols="80" >{{get('obsH','')}}</textarea>
</div>


<h3>(I) Habilidad para usar las técnicas, destrezas y herramientas
modernas para la práctica de la computación</h3>
<div>

%include inputGrades pid='I', vj1=get('gIj1',"0"), vj2=get('gIj2',"0"), vdir=get('gIdir',"0") 

Utilización adecuada de las herramientas y/o técnicas necesarias para
la realización del proyecto. Justificación de la tecnología/técnica
utilizada<br/>
<i>Observaciones:</i><br>
<textarea name="obsI" rows="3" cols="80" >{{get('obsI','')}}</textarea>
</div>


<h3>(J) Habilidad para aplicar los fundamentos y principios de las
matemáticas y de la computación en el modelamiento y diseño de
sistemas computacionales de manera que se demuestre comprensión de las
ventajas y desventajas en las decisiones de diseño</h3>
<div>

%include inputGrades pid='J1', vj1=get('gJ1j1',"0"), vj2=get('gJ1j2',"0"), vdir=get('gJ1dir',"0") 
(J1) Justificación y validez de los resultados y conclusiones.<br/>

%include inputGrades pid='J2', vj1=get('gJ2j1',"0"), vj2=get('gJ2j2',"0"), vdir=get('gJ2dir',"0") 
(J2) Correcta utilización de formalismos, técnicas y metodologías de
la disciplina<br/>

<i>Observaciones:</i><br>
<textarea name="obsJ" rows="3" cols="80" >{{get('obsJ','')}}</textarea>
</div>


<h3>(K) Habilidad para aplicar los principios de diseño y desarrollo
de software en la construcción de sistemas de diferente
complejidad</h3>
<div>
%include inputGrades pid='K', vj1=get('gKj1',"0"), vj2=get('gKj2',"0"), vdir=get('gKdir',"0") 

Validación de los resultados con respecto a los requerimientos y
rigurosidad en el seguimiento de la metodología<br/>
<i>Observaciones:</i><br>
<textarea name="obsK" rows="3" cols="80" >{{get('obsK','')}}</textarea>
</div>

</div> <!-- calificaciones -->

</form>

<br/>
<input type="button" id="vertabla" class="ui-state-default ui-corner-all" value="Ver/Ocultar tabla"/>

<input type="button" id="updatetabla" class="ui-state-default ui-corner-all" value="Actualizar tabla" onclick="updateTbl()"/>

<!-- <a href="#" id="vertabla" class="ui-state-default ui-corner-all">Ver/Ocultar tabla</a> -->

<div class="toggler">
  <div id="tablanotas" class="ui-widget-content ui-corner-all">

<center>
<form name="res">
<table border="1" cellspacing="0">
  <thead>  
  <tr>
    <th>Resultado de<br/>programa</th>
    <th colspan=2>Peso Relativo<br/>(1-5)</th> 
    <th>Jurado 1<br/>35%</th>
    <th>Jurado 2<br/>35%</th>
    <th>Director<br/>30%</th>
    <th>Nota<br/>Ponderada</th>
  </tr>
  <tbody>
  <tr>
    <td>A</td>
    <td>2</td>
    <td>5%</td>
    <td><input name="Aj1" readonly size="3"></td>
    <td><input name="Aj2" readonly size="3"></td>
    <td><input name="Adir" readonly size="3"></td>
    <td><input name="notaA" readonly size="3"></td>
  </tr>
  <tr>
    <td>B1</td>
    <td>2.5</td>
    <td>5%</td>
    <td><input name="B1j1" readonly size="3"></td>
    <td><input name="B1j2" readonly size="3"></td>
    <td><input name="B1dir" readonly size="3"></td>
    <td><input name="notaB1" readonly size="3"></td>
  </tr>
  <tr>
    <td>B2</td>
    <td>2.5</td>
    <td>5%</td>
    <td><input name="B2j1" readonly size="3"></td>
    <td><input name="B2j2" readonly size="3"></td>
    <td><input name="B2dir" readonly size="3"></td>
    <td><input name="notaB2" readonly size="3"></td>
  </tr>
  <tr>
    <td>C</td>
    <td>5</td>
    <td>12%</td>
    <td><input name="Cj1" readonly size="3"></td>
    <td><input name="Cj2" readonly size="3"></td>
    <td><input name="Cdir" readonly size="3"></td>
    <td><input name="notaC" readonly size="3"></td>
  </tr>
  <tr>
    <td>D</td>
    <td>5</td>
    <td>8%</td>
    <td><input name="Dj1" readonly size="3" value="N/A"></td>
    <td><input name="Dj2" readonly size="3" value="N/A"></td>
    <td><input name="Ddir" readonly size="3"></td>
    <td><input name="notaD" readonly size="3"></td>
  </tr>
  <tr>
    <td>E</td>
    <td>5</td>
    <td>10%</td>
    <td><input name="Ej1" readonly size="3"></td>
    <td><input name="Ej2" readonly size="3"></td>
    <td><input name="Edir" readonly size="3"></td>
    <td><input name="notaE" readonly size="3"></td>
  </tr>
  <tr>
    <td>F1</td>
    <td>2.5</td>
    <td>6%</td>
    <td><input name="F1j1" readonly size="3"></td>
    <td><input name="F1j2" readonly size="3"></td>
    <td><input name="F1dir" readonly size="3"></td>
    <td><input name="notaF1" readonly size="3"></td>
  </tr>
  <tr>
    <td>F2</td>
    <td>2.5</td>
    <td>6%</td>
    <td><input name="F2j1" readonly size="3"></td>
    <td><input name="F2j2" readonly size="3"></td>
    <td><input name="F2dir" readonly size="3"></td>
    <td><input name="notaF2" readonly size="3"></td>
  </tr>
  <tr>
    <td>G1</td>
    <td>2.5</td>
    <td>5%</td>
    <td><input name="G1j1" readonly size="3"></td>
    <td><input name="G1j2" readonly size="3"></td>
    <td><input name="G1dir" readonly size="3"></td>
    <td><input name="notaG1" readonly size="3"></td>
  </tr>
  <tr>
    <td>G2</td>
    <td>2.5</td>
    <td>5%</td>
    <td><input name="G2j1" readonly size="3"></td>
    <td><input name="G2j2" readonly size="3"></td>
    <td><input name="G2dir" readonly size="3"></td>
    <td><input name="notaG2" readonly size="3"></td>
  </tr>
  <tr>
    <td>H</td>
    <td>5</td>
    <td>11%</td>
    <td><input name="Hj1" readonly size="3" value="N/A"></td>
    <td><input name="Hj2" readonly size="3" value="N/A"></td>
    <td><input name="Hdir" readonly size="3"></td>
    <td><input name="notaH" readonly size="3"></td>
  </tr>
  <tr>
    <td>I</td>
    <td>2</td>
    <td>5%</td>
    <td><input name="Ij1" readonly size="3"></td>
    <td><input name="Ij2" readonly size="3"></td>
    <td><input name="Idir" readonly size="3"></td>
    <td><input name="notaI" readonly size="3"></td>
  </tr>
  <tr>
    <td>J1</td>
    <td>2.5</td>
    <td>6%</td>
    <td><input name="J1j1" readonly size="3"></td>
    <td><input name="J1j2" readonly size="3"></td>
    <td><input name="J1dir" readonly size="3"></td>
    <td><input name="notaJ1" readonly size="3"></td>
  </tr>
  <tr>
    <td>J2</td>
    <td>2.5</td>
    <td>6%</td>
    <td><input name="J2j1" readonly size="3"></td>
    <td><input name="J2j2" readonly size="3"></td>
    <td><input name="J2dir" readonly size="3"></td>
    <td><input name="notaJ2" readonly size="3"></td>
  </tr>
  <tr>
    <td>K</td>
    <td>3</td>
    <td>5%</td>
    <td><input name="Kj1" readonly size="3"></td>
    <td><input name="Kj2" readonly size="3"></td>
    <td><input name="Kdir" readonly size="3"></td>
    <td><input name="notaK" readonly size="3"></td>
  </tr>
</table>
</form>
<p>* Para los resultados de programa D y H la ponderación del Director
del Proyecto es del 100%.</p>
</center>

    </div>
</div>

<p>La calificación final queda sujeta a que se implementen las siguientes correcciones:</p>
<textarea form="grades" name="finalobs" rows="5" cols="80" >{{get('finalobs','')}}</textarea>


<p>Como resultado de las calificaciones parciales y sus ponderaciones, la calificación definitiva del Trabajo de Grado es: <span id="notadef"></span></p>

<hr/>

<p>Este trabajo puede ser considerado para MENCION DE HONOR : 
  <input form="grades" type="radio" name="mencion" value="si" onClick="$('#mencioninfo').toggle()"> Si
  <input form="grades" type="radio" name="mencion" value="no" checked  onClick="$('#mencioninfo').toggle()"> No
</p>

<div class="toggler">
  <div id="mencioninfo" class="ui-widget-content ui-corner-all">

<p>En atención a que el Trabajo de Grado se destaca por dos
condiciones (que indicamos) de las siguientes tres que indica el
artículo 2.28 de las Directrices de Trabajo de Grado:</p>

<ul>
<li>
  <input form="grades" type="checkbox" name="mencionitem1" value="1"
%if defined('mencionitem1') and mencionitem1=="1":
checked
%end
  > Los estudiantes superaron los objetivos propuestos.

<li>
  <input form="grades" type="checkbox" name="mencionitem2" value="1"
%if defined('mencionitem2') and mencionitem2=="1":
checked
%end
  > Los estudiantes demostraron una profundidad destacable en el
  conocimiento y tratamiento del tema.

<li>
  <input form="grades" type="checkbox" name="mencionitem3" value="1"
%if defined('mencionitem3') and mencionitem3=="1":
checked
%end
  > El tema ofrecía una dificultad superior a lo ordinario.
</ul>

<p>Los Jurados recomendamos que el Consejo de la Facultad otorgue
Mención de Honor a este Trabajo de Grado, y motivamos esta
recomendación con base en las siguientes apreciaciones:</p>
<textarea form="grades" name="obsmencion" rows="5" cols="80" >{{get('obsmencion','')}}</textarea>

  </div>
</div>

<script>
  $("#mencioninfo").hide();
</script>

<!-- <script> -->
  <!-- $("#tablanotas").hide(); -->
<!-- </script> -->

<hr/>
<input type="submit" form="grades" value="Enviar evaluación y generar PDF">
<input type="button" value="Cancelar" onclick="window.location.href='/tesisform/'"/>

</body>
</html>
