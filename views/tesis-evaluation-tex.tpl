\documentclass[letter,10pt]{article}

\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{fancyhdr}
\usepackage[top=4cm, bottom=3cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{xcolor}
\usepackage[none]{hyphenat}

\renewcommand{\headrulewidth}{0.0pt}

\fancyhf{}
\fancyhead{}
\fancyfoot{}

\definecolor{ObsColor}{rgb}{0.85,0.85,1}

\fancyhead[CO,CE]{\vspace{-1.2cm}
\begin{tabular}{p{.5\textwidth}p{.5\textwidth}}
\multirow{3}{*}{
\includegraphics[width=7cm]{../static/logo.png}
}
& \\
& {\bf Facultad de Ingenier\'ia} \\
& {\bf Ing. de Sistemas y Computaci\'on}\\\\
\hline
\end{tabular}
 }

 \fancyfoot[CO,CE]{ \hrule \textsc{\scriptsize\hfill Facultad de
     Ingenier\'ia. $\bullet$ Pontificia Universidad Javeriana Cali.
     $\bullet$ www.javerianacali.edu.co \hfill}}


\newcommand{\evalitem}[4]{\noindent ({\bf #1}) {\bf #2.} \emph{#3}.\\\\
Observaciones: \\ 
\colorbox{ObsColor}{
\begin{minipage}[h]{0.98\linewidth}
#4\hfill  
\end{minipage}}
\vspace{1cm}
}

\newcommand{\headtable}{
\begin{center}
\begin{tabular}{l p{5.5cm} lp{5.5cm} }
{\bf \large Acta No.} &  {{actn}} &{\bf \large Fecha:} & {{fecha}} \\%\cline{2-2}\cline{4-4}
\\
{\bf \large T\'itulo: } & \multicolumn{3}{p{13cm}}{ {{titulo}} } \\  %\cline{2-4}
\\
{\bf \large Autor 1:  } & {{aut1}} &{\bf \large Autor 2:}& {{aut2}} \\%\cline{2-2} \cline{4-4}
\\
{\bf \large Director: } & \multicolumn{3}{l}{ {{dir}} } \\  %\cline{2-4}
\\
{\bf \large Jurado 1:} & {{jur1}} &{\bf \large Jurado 2:} & {{jur2}} \\%\cline{2-2} \cline{4-4}
\\
\end{tabular}
\end{center}
}




\begin{document}
\pagestyle{fancy}
 
\begin{center}
{\bf \large ACTA DE CALIFICACI\'ON DE TRABAJO DE GRADO}\\
\end{center}

\headtable

\noindent En atenci\'on al desarrollo de este Trabajo de Grado y al documento y sustentaci\'on que presentaron los autores, los jurados damos las siguientes calificaciones parciales y observaciones:\\
 
\evalitem{A}{Habilidad para aplicar conocimientos de matem\'aticas,
  ciencias e ingenier\'ia}{Desarrollo, profundidad y rigor
  cient\'ifico en el tratamiento del tema. Interpretaci\'on de los
  resultados obtenidos}{ {{obsA}} }

\evalitem{B}{Habilidad para analizar un problema e identificar los
  requerimientos necesarios para su definici\'on y
  soluci\'on}{(B1)An\'alisis del problema a solucionar y formulaci\'on
  de hip\'otesis. (B2) Recopilaci\'on bibliogr\'afica del estado del
  arte del problema}{ {{obsB}} }

\evalitem{C}{Habilidad para dise\~nar, implementar y evaluar procesos
  y sistemas computacionales}{Dise\~no y creatividad de la soluci\'on
  propuesta}{ {{obsC}} }

\evalitem{D}{Habilidad para funcionar en equipos de
  trabajo}{Integraci\'on de diferentes puntos de vista para proponer
  soluciones}{ {{obsD}} }

\evalitem{E}{Entendimiento de la responsabilidad profesional y
  \'etica}{Adecuado cumplimiento de los objetivos del proyecto y
  puntualidad en las entregas}{ {{obsE}} }

\evalitem{F}{Habilidad para comunicarse efectivamente}{(F1) Calidad y
  redacci\'on del documento escrito. (F2) Solvencia de la
  presentaci\'on oral}{ {{obsF}} }

\evalitem{G}{Habilidad para analizar los impactos de la computaci\'on
  y la ingenier\'ia en las personas, organizaciones y la
  sociedad}{(G1)Justificaci\'on del proyecto. (G2) Identificaci\'on de
  los impactos de la soluci\'on en usuarios, comunidad cient\'ifica
  y/o aplicaciones derivadas}{ {{obsG}} }

\evalitem{H}{El reconocimiento de la necesidad de, y la habilidad
  para, continuar con el desarrollo profesional}{Actitud para aprender
  nuevo conocimiento y desaf\'io intelectual de la soluci\'on
  propuesta}{ {{obsH}} }

\evalitem{I}{Habilidad para usar las t\'ecnicas, destrezas y
  herramientas modernas para la pr\'actica de la
  computaci\'on}{Utilizaci\'on adecuada de las herramientas y/o
  t\'ecnicas necesarias para la realizaci\'on del
  proyecto. Justificaci\'on de la tecnolog\'ia/t\'ecnica utilizada}{
  {{obsI}} }

\evalitem{J}{Habilidad para aplicar los fundamentos y principios de
  las matem\'aticas y de la computaci\'on en el modelamiento y
  dise\~no de sistemas computacionales de manera que se demuestre
  comprensi\'on de las ventajas y desventajas en las decisiones de
  dise\~no}{(J1) Justificaci\'on y validez de los resultados y
  conclusiones. (J2) Correcta utilizaci\'on de formalismos, t\'ecnicas
  y metodolog\'ias de la disciplina}{ {{obsJ}} }

\evalitem{K}{Habilidad para aplicar los principios de dise\~no y
  desarrollo de software en la construcci\'on de sistemas de diferente
  complejidad}{Validaci\'on de los resultados con respecto a los
  requerimientos y rigurosidad en el seguimiento de la metodolog\'ia}{
  {{obsK}} }

\begin{center}
{\small
\begin{tabular}{| c | c | c | c | c | c | c | c | }
\hline
{\bf Resultado de} & \multicolumn{2}{c|}{\bf Peso Relativo } 
& {\bf Jurado 1} & {\bf Jurado 2} 
& {\bf Director} & {\bf Nota} \\
{\bf Programa}&\multicolumn{2}{c|}{\bf (1-5)} & {\bf 35\%}& {\bf 35\%}& {\bf 30\%*}& {\bf Ponderada}\\ \hline
A & 2 & 5\% & {{gAj1}} &  {{gAj2}} &  {{gAdir}} & {{gA}} \\
\hline
B1 & 2.5 & 5\% & {{gB1j1}} & {{gB1j2}} & {{gB1dir}} & {{gB1}}\\
\hline
B2 & 2.5 & 5\% & {{gB2j1}} & {{gB2j2}} & {{gB2dir}} & {{gB2}}\\
\hline
C & 5 & 12\% & {{gCj1}} & {{gCj2}} & {{gCdir}} & {{gC}}\\ 
\hline
D & 5 & 8\% & N/A & N/A& {{gDdir}} & {{gD}}\\ 
\hline
E & 5 & 10\% & {{gEj1}} & {{gEj2}} & {{gEdir}} & {{gE}}\\ 
\hline
F1 & 2.5 & 6\% & {{gF1j1}} & {{gF1j2}} & {{gF1dir}} & {{gF1}}\\ 
\hline
F2 & 2.5 & 6\% & {{gF2j1}} & {{gF2j2}} & {{gF2dir}} & {{gF2}}\\ 
\hline
G1 & 2.5 & 5\% & {{gG1j1}} & {{gG1j2}} & {{gG1dir}} & {{gG1}}\\ 
\hline
G2 & 2.5 & 5\% & {{gG2j1}} & {{gG2j2}} & {{gG2dir}} & {{gG2}}\\ 
\hline
H & 5 & 11\% & N/A& N/A& {{gHdir}} & {{gH}}\\ 
\hline
I & 2 & 5\% & {{gIj1}} & {{gIj2}} & {{gIdir}} & {{gI}}\\ 
\hline
J1 & 2.5 & 6\% & {{gJ1j1}} & {{gJ1j2}} & {{gJ1dir}} & {{gJ1}}\\ 
\hline
J2 & 2.5 & 6\% & {{gJ2j1}} & {{gJ2j2}} & {{gJ2dir}} & {{gJ2}}\\ 
\hline
K & 3 & 5\% & {{gKj1}} & {{gKj2}} & {{gKdir}} & {{gK}}\\ 
\hline
\end{tabular}
}
\end{center}
* Para los resultados de programa D y H la ponderaci\'on del Director del Proyecto es del 100\%. \\

\newpage

\begin{center} 
\fbox{\fbox{\parbox{16cm}{
\noindent{\bf Como resultado de las calificaciones parciales y sus ponderaciones, la calificaci\'on definitiva del Trabajo de Grado es: \\\\}
\begin{tabular}{lp{2cm}lp{9cm}}
{Nota Definitiva:} & {\textbf{\large{{gdef}}}} & & \\\cline{2-2}\cline{4-4}
& N\'umeros & &  Letras 
\end{tabular}
}}}
\end{center}

\noindent
La calificaci\'on final queda sujeta a que se implementen las siguientes correcciones: \\\\
\noindent
\colorbox{ObsColor}{
\begin{minipage}[h]{0.98\linewidth}
{{finalobs}}
\vspace{3cm}\hfill  
\end{minipage}}

\vspace{2cm}

\hbox to 150pt{\hrulefill}
\noindent {{jur1}}\\\\\\

\hbox to 150pt{\hrulefill}
\noindent {{jur2}}\\\\\\

\hbox to 150pt{\hrulefill}
\noindent {{dir}}\\\\\\

\hbox to 150pt{\hrulefill}
\noindent Andr\es Adolfo Navarro Newball\\
\noindent Director Carrera.\\
Ingenier\'ia de Sistemas y Computaci\'on.\\\\


%if mencion == 'si':

\newpage
\begin{center}
{\large \bf RECOMENDACI\'ON DE MENCI\'ON DE HONOR}
\end{center}

\ \\ \headtable

En atenci\'on a que el Trabajo de Grado se destaca por dos condiciones
(que indicamos) de las siguientes tres que indica el art\'iculo 2.28
de las Directrices de Trabajo de Grado:
\begin{itemize}
\item[ 
%if defined('mencionitem1') and mencionitem1=="1":
$\blacksquare$
%else:
$\square$
%end
] Los estudiantes superaron los objetivos propuestos.

\item[ 
%if defined('mencionitem2') and mencionitem2=="1":
$\blacksquare$
%else:
$\square$
%end
] Los estudiantes demostraron una profundidad destacable en el
  conocimiento y tratamiento del tema.

\item[ 
%if defined('mencionitem3'):
$\blacksquare$
%else:
$\square$
%end
] El tema ofrec\'ia una dificultad superior a lo ordinario.

\end{itemize}

\noindent Los Jurados recomendamos que el Consejo de la Facultad otorgue Menci\'on de Honor a este Trabajo de Grado, y motivamos esta recomendaci\'on con base en las siguientes apreciaciones: \\\\
\colorbox{ObsColor}{
\begin{minipage}[h]{0.98\linewidth}
{{obsmencion}}\hfill  
\vspace{2cm}
\end{minipage}}

\vspace{2cm}

\begin{tabular}{p{6cm} p{2cm} p{6cm} }
\cline{1-1} \cline{3-3} 
{{jur1}} & & {{jur2}}
\end{tabular}
\ \\ 
\begin{center} 
\fbox{\fbox{\parbox{16cm}{
\noindent{\bf Decisi\'on del Consejo de la Facultad:}\\
\noindent En virtud de las condiciones que indicaron los Jurados y su motivaci\'on, el Consejo de la Facultad decidi\'o lo siguiente: \\
\noindent Conceder Menci\'on de Honor al Trabajo de Grado \hbox 
to 2cm{\enspace\hrulefill}\\
Tal y como se consigna en el Acta No.   \hbox to 2cm{\hrulefill}   del  \hbox to 3cm{\hrulefill} del Consejo de la Facultad.
}}}
\end{center}

%end

\end{document}
