<table>
<tr>
  <td><b>Acta No.</b></td>
  <td>{{actn}}</td>
  <td><b>Fecha:</b></td>
  <td>{{fecha or ''}}</td>
</tr>
<tr> 
  <td><b>Título:</b></td>
  <td colspan="3">{{titulo}}</td>
</tr>
<tr>
  <td><b>Autor 1:</b></td>
  <td>{{aut1}}</td>
  <td><b>Autor 2:</b></td>
  <td>{{aut2}}</td>
</tr>
<tr>
  <td><b>Director:</b></td>
  <td>{{dir}}</td>
</tr>
<tr>
  <td><b>Jurado 1:</b></td>
  <td>
%if defined('jur1'):
{{jur1}}
%end
</td>
  <td><b>Jurado 2:</b></td>
  <td>
%if defined('jur2'):
{{jur2}}
%end
</td>
</tr>
</table>
