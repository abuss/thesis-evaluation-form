<html>
<body>
%include header
<hr>

<h2>RECOMENDACIÓN DE MENCIÓN DE HONOR</h2>

%include headtable actn=actn, fecha=fecha, titulo=titulo, aut1=aut1, aut2=aut2, dir=dir, jur1=jur1, jur2=jur2

<p>En atención a que el Trabajo de Grado se destaca por dos condiciones (que indicamos) de las siguientes tres que indica el artículo 2.28 de las Directrices de Trabajo de Grado:</p>

<form>
<lu>
 <li><input type="checkbox" name="condiciones" value="cond1"/> Los estudiantes superaron los objetivos propuestos.</li>
 <li><input type="checkbox" name="condiciones" value="cond2"/> Los estudiantes demostraron una profundidad destacable en el  conocimiento y tratamiento del tema.</li>
 <li><input type="checkbox" name="condiciones" value="cond3"/> El tema ofrecía una dificultad superior a lo ordinario.</li>
</lu>

<p>Los Jurados recomendamos que el Consejo de la Facultad otorgue Mención de Honor a este Trabajo de Grado, y motivamos esta recomendación con base en las siguientes apreciaciones:</p>
<textarea name="menobs" rows="4" cols="80"> 
</textarea><br>
<input type='submit'>
</form>

<!--
<table>
\begin{tabular}{p{6cm} p{2cm} p{6cm} }
\cline{1-1} \cline{3-3} 
JURADO1 & & JURADO2
\end{tabular}
</table>
-->

<!-- 
\begin{center} 
\fbox{\fbox{\parbox{16cm}{
\noindent{\bf Decisión del Consejo de la Facultad:}\\
\noindent En virtud de las condiciones que indicaron los Jurados y su motivación, el Consejo de la Facultad decidió lo siguiente: \\
\noindent Conceder Mención de Honor al Trabajo de Grado \hbox 
to 2cm{\enspace\hrulefill}\\
Tal y como se consigna en el Acta No.   \hbox to 2cm{\hrulefill}   del  \hbox to 3cm{\hrulefill} del Consejo de la Facultad.
}}}
\end{center}
-->
</body>
</html>
