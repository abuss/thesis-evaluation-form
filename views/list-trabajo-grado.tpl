<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Evaluación the tesis de grado: Lista de actas</title>
</head>
<body>

%include header
<hr>

<h2>ACTA DE CALIFICACION DE TRABAJO DE GRADO</h2>


<p>Listado de actas de trabajos de grado registrados:</p>
<table>
  <tr><th>Acta #</th><th>Titulo</th>
%for t in listact:
  <tr>
    <td>
      {{t['actn']}}
    </td>
    <td>
      <a href='tesis?actn={{t['actn']}}'>{{t['titulo']}}</a>
    </td>
  </tr>
%end
</table>
<hr/>
<input type="button" id="crearacta" class="ui-state-default ui-corner-all" value="Crear nueva acta" onclick="window.location.href='new'"/>

</body>
</html>
