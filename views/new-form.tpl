<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Evaluación the tesis de grado: Nueva acta</title>
</head>
<body>

%include header

<hr>
<h2>ACTA DE CALIFICACIÓN DE TRABAJO DE GRADO</h2>

<form action="create" method="POST">
  <table>
    <tr>
      <td><b>Acta No.</b></td>
      <td><input name="actn"/></td>
      <td><b>Fecha:</b></td>
      <td><input name="fecha" type="date"/></td>
    </tr>
    <tr> 
      <td><b>Título:</b></td>
      <td colspan="3"><input name="titulo" size="80"></td>
    </tr>
    <tr>
      <td><b>Autor 1:</b></td>
      <td><input name="aut1"></td>
      <td><b>Autor 2:</b></td>
      <td><input name="aut2"></td>
    </tr>
    <tr>
      <td><b>Director:</b></td>
      <td><input name="dir"></td>
    </tr>
    <tr>
      <td><b>Jurado 1:</b></td>
      <td><input name="jur1"></td>
      <td><b>Jurado 2:</b></td>
      <td><input name="jur2"></td>
    </tr>
  </table>
  <hr/>
  <input type="submit" value="Crear">
  <input type="button" value="Cancelar" onclick="window.location.href='/tesisform/'"/>
</form>

</body>
</table>
